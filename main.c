#include <termios.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <stdlib.h>
#include <curses.h>
#include <ncurses.h>
#include <stdlib.h>

void sig_winch(int signo) {
    struct winsize size;
    ioctl(fileno(stdout), TIOCGWINSZ, (char *) &size);
    resizeterm(size.ws_row, size.ws_col);
}
typedef struct
{
    int x;
    int y;
    int height;
    int width;
    char border_left;
    char border_top;
} WIN_DATA;
WINDOW * create_win(WIN_DATA * wd);
int destroy_win(WINDOW * w);

int main(int argc, char * argv[])
{
    int row_max = 0, col_max = 0;
    WIN_DATA main_wd;
    initscr();
    raw();
    noecho();
    getmaxyx(stdscr, row_max, col_max);
    memset(&main_wd, 0, sizeof(WIN_DATA));
    main_wd.width = row_max;
    main_wd.height = col_max;
    WINDOW *main_win = create_win(&main_wd);
    signal(SIGWINCH, sig_winch);
    curs_set(0);
    start_color();
    init_pair(1, COLOR_BLUE, COLOR_BLACK);
    wbkgd(main_win, COLOR_PAIR(1));
    getch();
    destroy_win(main_win);
    endwin();
    return 0;
}

WINDOW * create_win(WIN_DATA * wd)
{
    WINDOW * wptr = NULL;
    wptr =  newwin(wd->height, wd->width, wd->y, wd->x);
    box(wptr, 0, 0);
    wrefresh(wptr);

    return wptr;
}

int destroy_win(WINDOW * w) {
    wborder(w, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
    wrefresh(w);
    return delwin(w);
}
